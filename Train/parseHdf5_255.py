import numpy as np
import scipy.ndimage
import sys
from ast import literal_eval as make_tuple

sys.path.insert(0, './utils')
sys.path.insert(0, './model')
from store2hdf5 import store2hdf53D

import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('-f', '--reference', help='Reference image filename (required)', type=str, action='append', required = True)
    parser.add_argument('-o', '--output', help='Name of output HDF5 files (required)', type=str, action='append', required = True)
    parser.add_argument('-s', '--scale',  help='Scale factor (default = 2,2,2). Append mode: -s 2,2,2 -s 3,3,3 ', type=str, action='append')
    parser.add_argument('--stride', help='Indicates step size at which extraction shall be performed (default=10)', type=int, default=10)
    parser.add_argument('-b','--batch', help='Indicates batch size for HDF5 storage', type=int, default=256)
    parser.add_argument('-p','--patchsize', help='Indicates input patch size for extraction', type=int, default=33)
    parser.add_argument('-m', '--model', help='Models for training : 9-1-5, 9-3-5, 9-5-5. (default=9-1-5)', type=str, default='9-1-5')   
    parser.add_argument('--border', help='Border to remove (default=10,10,0)', type=str, default='10,10,0')
    parser.add_argument('--order', help='Order of spline interpolation (default=3) ', type=int, default=3)
    parser.add_argument('--samples', help='Indicates limit of samples in HDF5 file (optional)', type=int)
    parser.add_argument('--sigma', help='Standard deviation (sigma) of Gaussian blur (default=1)', type=int, default=1)
    parser.add_argument('-t', '--text', help='Name of a text (.txt) file which contains HDF5 file names (default: model/train.txt)', type=str, default='model/train.txt')
    parser.add_argument('-n', '--netname', help='Name of train netwotk protocol (default=model/SRCNN3D_net.prototxt)', type=str, default='model/SRCNN3D_net.prototxt')
    parser.add_argument('-d', '--deployname', help='Name of deploy files in order to deploy the parameters of SRCNN3D_net without reading HDF5 files (default=model/SRCNN3D_deploy.prototxt)', type=str, default='model/SRCNN3D_deploy.prototxt')
    
    args = parser.parse_args()


    for i in range(0,len(args.output)):

        hdf5name = args.output[i]

        # Reading HDF5 file                           
        import h5py
        with h5py.File(hdf5name,'r+') as hf:
            udata = hf.get('data')
            print 'Shape of interpolated low-resolution patches:', udata.shape
            print 'Chunk (batch) of interpolated low-resolution patches:', udata.chunks
            ulabel = hf.get('label')
            print 'Shape of reference high-resolution patches:', ulabel.shape
            print 'Chunk (batch) of reference high-resolution patches:', ulabel.chunks

            udata[:] = udata[:]*255.0
            ulabel[:] = ulabel[:]*255.0

            hf.close()

            #hf.__setitem__('data',udata)
