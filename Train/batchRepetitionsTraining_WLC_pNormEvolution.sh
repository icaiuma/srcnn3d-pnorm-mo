#!/bin/bash
# Script para ejecutar el entrenamiento de SRCNN3D variando la norma p de la capa Loss
# incluyendo una ponderación con la Euclidean loss layer dada por beta


# Defino la path para que me coja la nueva capa Loss de Python
#export PYTHONPATH=/home/karlkhader/Documents/deepBrain-master/SRCNN3D/Train/layers/:${PYTHONPATH}
export PYTHONPATH=`pwd`/layers/:${PYTHONPATH}

# Defino los valores de p a ejecutar
p=1.9
beta=0.35

for i in `seq 1 10`; do

    cp ../../ICAE/TrainImages/train${i}.txt model/trainOASIS.txt
    cp ../../ICAE/TrainImages/val${i}.txt model/valOASIS.txt

    mkdir -p caffe_model/OASIS/WLC/train${i}
    # Uso el comando bc para calcular 1-beta
    alpha=0`bc <<< 1-${beta}`

    # Modifico los archivos de configuración y sufijos para la salida
    sed -i "/snapshot_prefix*/ c\snapshot_prefix: \"caffe_model/OASIS/WLC/train${i}/SRCNN3D_train${i}\" " model/SRCNN3D_WLC_OASIS_solver.prototxt
    sed -i "/layer: 'PnormLossLayer'/,/param_str:*/ c\    layer: \'PnormLossLayer\'\n    param_str: \'${p}\'" model/SRCNN3D_WLC_OASIS_net.prototxt
    sed -i "/# Euclidean loss weight/,/loss_weight:*/ c\  # Euclidean loss weight\n  loss_weight: ${alpha}" model/SRCNN3D_WLC_OASIS_net.prototxt
    sed -i "/# p-norm loss weight/,/loss_weight:*/ c\  # p-norm loss weight\n  loss_weight: ${beta}" model/SRCNN3D_WLC_OASIS_net.prototxt

    # Ejecuto entrenamiento
    caffe train -gpu=0 --solver model/SRCNN3D_WLC_OASIS_solver.prototxt 2>&1 | tee caffe_model/OASIS/WLC/model_train${i}.log

done

