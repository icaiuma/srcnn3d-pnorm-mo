#!/bin/bash
# Script para ejecutar el entrenamiento de SRCNN3D según el artículo


# Creo variable con la ruta hacia las imágenes
Dataset=/home/karlkhader/Documents/deepBrain-master/TrainImages

# Ejecuto un bucle para hacer crear entrada con los nombres de los ficheros
FICHEROS=`dir $Dataset/*.nii`
ENTRADA=''
for fich in $FICHEROS; 
do
#path=${fich%/*}
file=${fich##*/}
base=${file%%.*}
ENTRADA="${ENTRADA} -f ${fich} -o hdf5/${base}.hdf5"
done

python generate_training.py ${ENTRADA} -p 25 -b 256 -m '9-1-5' -s 2,2,2 -s 3,3,3

# Ejecuto la extracción de patches
#python generate_training.py -f $Dataset/KKI2009-33-MPRAGE.nii.gz -o hdf5/KKI2009-33-MPRAGE.hdf5 -f $Dataset/KKI2009-34-MPRAGE.nii.gz -o hdf5/KKI2009-34-MPRAGE.hdf5 -f $Dataset/KKI2009-35-MPRAGE.nii.gz -o hdf5/KKI2009-35-MPRAGE.hdf5 -f $Dataset/KKI2009-36-MPRAGE.nii.gz -o hdf5/KKI2009-36-MPRAGE.hdf5 -f $Dataset/KKI2009-37-MPRAGE.nii.gz -o hdf5/KKI2009-37-MPRAGE.hdf5 -f $Dataset/KKI2009-38-MPRAGE.nii.gz -o hdf5/KKI2009-38-MPRAGE.hdf5 -f $Dataset/KKI2009-39-MPRAGE.nii.gz -o hdf5/KKI2009-39-MPRAGE.hdf5 -f $Dataset/KKI2009-40-MPRAGE.nii.gz -o hdf5/KKI2009-40-MPRAGE.hdf5 -f $Dataset/KKI2009-41-MPRAGE.nii.gz -o hdf5/KKI2009-41-MPRAGE.hdf5 -f $Dataset/KKI2009-42-MPRAGE.nii.gz -o hdf5/KKI2009-42-MPRAGE.hdf5 -p 25 -b 256 -m '9-1-5' -s 2,2,2 -s 3,3,3

# Creo prototxt
python generate_solver.py -l 0.0001 -m 1500000

# Ejecuto entrenamiento
export PYTHONPATH=/home/karlkhader/Documentos/deepBrain-master/SRCNN3D/Train/layers/:${PYTHONPATH}
caffe train -gpu=0 --solver model/SRCNN3D_solver.prototxt 2>&1 | tee caffe_model/model_train.log
#~/caffe/build/tools/caffe train -gpu=0 --solver model/SRCNN3D_solver.prototxt | tee caffe_model/model_train.log

# Resumo resultados en un txt y muestro gráficas (https://cvdreamer.wordpress.com/2016/07/27/how-to-plot-training/)
python plot_utils/plot_training_log.py 6 caffe_model/train.png caffe_model/model_train.log
#parse_log.sh caffe_model/model_train.log
