#!/bin/bash
# Script para ejecutar el entrenamiento de SRCNN3D variando la norma p de la capa Loss


# Defino la path para que me coja la nueva capa Loss de Python
export PYTHONPATH=/home/karlkhader/Documents/deepBrain-master/SRCNN3D/Train/layers/:${PYTHONPATH}

# Defino los valores de p a ejecutar
#p_values=(0.5 0.75 1 2 3 4 5 6 7 8 9)
p_values=(1.6 1.9)

for p in ${p_values[@]}; do

    mkdir caffe_model/p_${p}

    # Modifico los archivos de configuración y sufijos para la salida
    sed -i "/snapshot_prefix*/ c\snapshot_prefix: \"caffe_model/p_${p}/SRCNN3D_${p}\" " model/SRCNN3D_solver.prototxt
    sed -i "/layer: 'PnormLossLayer'/,/param_str:*/ c\    layer: \'PnormLossLayer\'\n    param_str: \'${p}\'" model/SRCNN3D_net.prototxt

    # Ejecuto entrenamiento
    caffe train -gpu=1 --solver model/SRCNN3D_solver.prototxt 2>&1 | tee caffe_model/model_train_${p}.log

done

