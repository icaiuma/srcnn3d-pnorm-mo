#!/bin/bash
# Script para ejecutar el entrenamiento de SRCNN3D según el artículo


# Creo variable con la ruta hacia las imágenes
Dataset=/home/karlkhader/Documents/deepBrain-master/TrainImages

# Ejecuto un bucle para hacer crear entrada con los nombres de los ficheros
FICHEROS=`dir $Dataset/*.nii`
ENTRADA=''
for fich in $FICHEROS; 
do
#path=${fich%/*}
file=${fich##*/}
base=${file%%.*}
ENTRADA="${ENTRADA} -f ${fich} -o hdf5/hdf5_255/${base}.hdf5"
done

python parseHdf5_255.py ${ENTRADA} -p 25 -b 256 -m '9-1-5' -s 2,2,2 -s 3,3,3