#!/bin/bash
# Script para ejecutar el entrenamiento de SRCNN3D variando la norma p de la capa Loss
# incluyendo una ponderación con la Euclidean loss layer dada por beta


# Defino la path para que me coja la nueva capa Loss de Python
#export PYTHONPATH=/home/karlkhader/Documents/deepBrain-master/SRCNN3D/Train/layers/:${PYTHONPATH}
export PYTHONPATH=`pwd`/layers/:${PYTHONPATH}

# Defino los valores de p a ejecutar
#p_values=(0.5 0.75 1 2 3 4 5 6 7 8 9)
p_values=(1.9)
beta_values=(0.5)

for p in ${p_values[@]}; do

    for beta in ${beta_values[@]}; do

        mkdir caffe_model/OASIS/p_${p}
        mkdir caffe_model/OASIS/p_${p}/beta_${beta}
        # Uso el comando bc para calcular 1-beta
        alpha=0`bc <<< 1-${beta}`

        # Modifico los archivos de configuración y sufijos para la salida
        sed -i "/snapshot_prefix*/ c\snapshot_prefix: \"caffe_model/OASIS/p_${p}/beta_${beta}/SRCNN3D_${p}_${beta}\" " model/SRCNN3D_WLC_OASIS_solver.prototxt
        sed -i "/layer: 'PnormLossLayer'/,/param_str:*/ c\    layer: \'PnormLossLayer\'\n    param_str: \'${p}\'" model/SRCNN3D_WLC_OASIS_net.prototxt
        sed -i "/# Euclidean loss weight/,/loss_weight:*/ c\  # Euclidean loss weight\n  loss_weight: ${alpha}" model/SRCNN3D_WLC_OASIS_net.prototxt
        sed -i "/# p-norm loss weight/,/loss_weight:*/ c\  # p-norm loss weight\n  loss_weight: ${beta}" model/SRCNN3D_WLC_OASIS_net.prototxt

        # Ejecuto entrenamiento
        caffe train -gpu=0 --solver model/SRCNN3D_WLC_OASIS_solver.prototxt 2>&1 | tee caffe_model/OASIS/model_train_${p}_${beta}.log
    done

done

