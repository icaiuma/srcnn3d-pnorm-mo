#!/bin/bash
# Script para ejecutar el entrenamiento de SRCNN3D variando la norma p de la capa Loss
# incluyendo una ponderación con la Euclidean loss layer dada por beta


# Defino la path para que me coja la nueva capa Loss de Python
#export PYTHONPATH=/home/karlkhader/Documents/deepBrain-master/SRCNN3D/Train/layers/:${PYTHONPATH}
export PYTHONPATH=`pwd`/layers/:${PYTHONPATH}

# Defino los valores de p a ejecutar
#p_values=(0.5 0.75 1 2 3 4 5 6 7 8 9)
p=2
# beta y alpha no tienen por qué sumar uno, luego los pongo separados para libre elección

for i in `seq 1 10`; do

    cp ../../ICAE/TrainImages/train${i}.txt model/trainOASIS_pNorm.txt
    cp ../../ICAE/TrainImages/val${i}.txt model/valOASIS_pNorm.txt

    mkdir -p caffe_model/OASIS/p_${p}/train${i}

    # Modifico los archivos de configuración y sufijos para la salida
    sed -i "/snapshot_prefix*/ c\snapshot_prefix: \"caffe_model/OASIS/p_${p}/train${i}/SRCNN3D_train${i}\" " model/SRCNN3D_OASIS_solver.prototxt
    sed -i "/layer: 'PnormLossLayer'/,/param_str:*/ c\    layer: \'PnormLossLayer\'\n    param_str: \'${p}\'" model/SRCNN3D_OASIS_net.prototxt

    # Ejecuto entrenamiento
    caffe train -gpu=1 --solver model/SRCNN3D_OASIS_solver.prototxt 2>&1 | tee caffe_model/OASIS/p_${p}/model_train${i}.log

done

