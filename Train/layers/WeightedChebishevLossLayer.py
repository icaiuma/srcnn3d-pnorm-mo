import caffe
import numpy as np
import json
#from ast import literal_eval

class WeightedChebishevLossLayer(caffe.Layer):
    """
    Compute the P-norm Loss.
    """

    def setup(self, bottom, top):
        # check input pair
        if len(bottom) != 2:
            raise Exception("Need two inputs to compute distance.")
        
        # get aditional parameter no define p-norm
        #params = np.array(literal_eval(self.param_str)) # "[1.2,1.5,0.2,0.8]" 
        params = json.loads(self.param_str) #'{"pNorms":[1, 2], "weights":[0.2, 0.8]}'
        self.pNorms = params["pNorms"]
        self.weights = params["weights"]
        self.losses = []

        if len(self.pNorms) != len(self.weights):
            raise Exception("pNorms and weights must have the same dimension.")


    def reshape(self, bottom, top):
        # check input dimensions match
        if bottom[0].count != bottom[1].count:
            raise Exception("Inputs must have the same dimension.")
        # difference is shape of inputs
        self.diff = np.zeros_like(bottom[0].data, dtype=np.float32)
        # loss output is scalar
        top[0].reshape(1)

    def forward(self, bottom, top):
        self.diff[...] = bottom[0].data - bottom[1].data

        losses = []
        for i in range(len(self.pNorms)):
            pNormLoss = float(self.weights[i]) * np.sum(np.absolute(self.diff)**float(self.pNorms[i]))\
                 / bottom[0].num / float(self.pNorms[i])
            losses.append(pNormLoss)
            
        self.losses = losses
        top[0].data[...] = np.amax(self.losses)
        # print(top[0].data)

    def backward(self, top, propagate_down, bottom):
        for i in range(2):
            if not propagate_down[i]:
                continue
            if i == 0:
                sign = 1
            else:
                sign = -1

            bottom[i].diff[...] = maxDerivative(self.diff,bottom[i].num,sign,self.pNorms,self.weights,self.losses)


def maxDerivative(diff, num, sign, pNorms, weights, losses):
        if len(pNorms) == 1:
            return sign * float(weights[0]) * \
                (np.sign(diff)*(np.absolute(diff)**(float(pNorms[0])-1))) / num
        
        else:
            pnormDrv =  sign * float(weights[0]) * \
                (np.sign(diff)*(np.absolute(diff)**(float(pNorms[0])-1))) / num
            restpNormDrv = maxDerivative(diff,num,sign,pNorms[1:],weights[1:],losses[1:])
            maxDrv = (pnormDrv + restpNormDrv + \
                np.sign(losses[0]-np.amax(losses[1:]))*(pnormDrv-restpNormDrv)) / 2.
            return maxDrv