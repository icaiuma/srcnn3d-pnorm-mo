close all

p = [2 4];

for ThisP = p

    %% Load data
    % Filename
    filename = sprintf('../results/model_train_%s_255.log.train',num2str(ThisP));
    % Format for each line of text:
    formatSpec = '%f%f%f%f%[^\n\r]';
    delimiter = ' ';
    startRow = 2;
    % Open the text file.
    fileID = fopen(filename,'r');
    % Read columns of data according to the format.
    dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'MultipleDelimsAsOne', true, 'TextType', 'string', 'EmptyValue', 1.0E-4, 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
    % Close the text file.
    fclose(fileID);
    % Create output variable
    modeltrain = table(dataArray{1:end-1}, 'VariableNames', {'Iters','Seconds','TrainingLoss','LearningRate'});
    % Clear temporary variables
    clearvars filename delimiter startRow formatSpec fileID dataArray ans;


    %% Plot results
    % Defino filtro de mediana m�vil
    windowSize = 100; 
%     b = (1/windowSize)*ones(1,windowSize);
%     a = 1;
%     % Aplico filtro
%     y = filter(b,a,modeltrain.TrainingLoss);

    y = smooth(modeltrain.TrainingLoss,windowSize);

    figure
    plot(modeltrain.Iters,modeltrain.TrainingLoss)
    hold on
    plot(modeltrain.Iters,y)
    legend('Input Data','Filtered Data')
    title(['p = ',num2str(ThisP)])
    
    savefig(sprintf('../results/model_train_%s_255.fig',num2str(ThisP)))
    
end