close all

% p = [0.5 0.75 1 2 3 4 5 6 7 8 9];
p = [1.3];
colors = distinguishable_colors(numel(p));
% colors = gray(numel(p)+1);


for NdxP = 1:numel(p)

    ThisP = p(NdxP);
    
    %% Load data
    % Filename
    filename = sprintf('../results/model_train_%s.log.train',num2str(ThisP));
    % Format for each line of text:
    formatSpec = '%f%f%f%f%[^\n\r]';
    delimiter = ' ';
    startRow = 2;
    % Open the text file.
    fileID = fopen(filename,'r');
    % Read columns of data according to the format.
    dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'MultipleDelimsAsOne', true, 'TextType', 'string', 'EmptyValue', 1.0E-4, 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
    % Close the text file.
    fclose(fileID);
    % Create output variable
    modeltrain = table(dataArray{1:end-1}, 'VariableNames', {'Iters','Seconds','TrainingLoss','LearningRate'});
    % Clear temporary variables
    clearvars filename delimiter startRow formatSpec fileID dataArray ans;


    %% Plot results
    % Defino filtro de mediana m�vil
    windowSize = 50; 
%     b = (1/windowSize)*ones(1,windowSize);
%     a = 1;
%     % Aplico filtro
%     y = filter(b,a,modeltrain.TrainingLoss);

    y = smooth(modeltrain.TrainingLoss,windowSize);

    figure
    plot(modeltrain.Iters,modeltrain.TrainingLoss)
    hold on
    plot(modeltrain.Iters,y)
    legend('Input Data','Filtered Data')
    title(['p = ',num2str(ThisP)])
    hold off
    
%     savefig(sprintf('../results/model_train_%s.fig',num2str(ThisP)))
    
    
    figure(2)
    hold on
    plot(modeltrain.Iters,y,'Color',colors(NdxP,:),'LineWidth',2)
    hold off
    
    
end

figure(2)
legend(strcat('p = ',cellfun(@num2str,num2cell(p),'uniformoutput',false)))
xlabel('iteration')
ylabel('p-norm loss')
set(gca,'YScale','log')
% PdfFileName=sprintf('../results/model_train_loss.pdf');
% set(gcf,'PaperUnits','centimeters');
% set(gcf,'PaperOrientation','portrait');
% set(gcf,'PaperPositionMode','manual');
% set(gcf,'PaperSize',[23 20]);
% set(gcf,'PaperPosition',[0 0 23 20]);
% set(gca,'fontsize',20);
% saveas(gcf,PdfFileName,'pdf');
