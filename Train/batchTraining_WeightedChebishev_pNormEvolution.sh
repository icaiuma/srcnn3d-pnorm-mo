#!/bin/bash
# Script para ejecutar el entrenamiento de SRCNN3D variando la norma p de la capa Loss
# incluyendo una ponderación con la Euclidean loss layer dada por beta


# Defino la path para que me coja la nueva capa Loss de Python
#export PYTHONPATH=/home/karlkhader/Documents/deepBrain-master/SRCNN3D/Train/layers/:${PYTHONPATH}
export PYTHONPATH=`pwd`/layers/:${PYTHONPATH}

# Defino los valores de p a ejecutar
#p_values=(0.5 0.75 1 2 3 4 5 6 7 8 9)
fixed_pNorm=2
p_values=(2.1)
# beta y alpha no tienen por qué sumar uno, luego los pongo separados para libre elección
alpha_values=(1 1 1 1 1 1 1)
beta_values=(0.65 0.7 0.75 0.8 0.85 0.9 1)

for p in ${p_values[@]}; do

    #for beta in ${beta_values[@]}; do
    for i in ${!beta_values[@]}; do

        beta=${beta_values[$i]}
        alpha=${alpha_values[$i]}

        mkdir caffe_model/WeightedChebishev/p_${p}_f${fixed_pNorm}
        mkdir caffe_model/WeightedChebishev/p_${p}_f${fixed_pNorm}/beta_${beta}_alpha_${alpha}
        # Uso el comando bc para calcular 1-beta y concateno el 0 delante
        #alpha=0`bc <<< 1-${beta}`

        # Modifico los archivos de configuración y sufijos para la salida
        sed -i "/snapshot_prefix*/ c\snapshot_prefix: \"caffe_model/WeightedChebishev/p_${p}_f${fixed_pNorm}/beta_${beta}_alpha_${alpha}/SRCNN3D_${p}_${beta}\" " model/SRCNN3D_WeightedChebishev_solver.prototxt
        sed -i "/layer: 'WeightedChebishevLossLayer'/,/param_str:*/ c\    layer: \'WeightedChebishevLossLayer\'\n    param_str: \"{\x5c\"pNorms\x5c\":[${fixed_pNorm}, ${p}], \x5c\"weights\x5c\":[${alpha}, ${beta}]}\"" model/SRCNN3D_WeightedChebishev_net.prototxt

        # Ejecuto entrenamiento
        caffe train -gpu=1 --solver model/SRCNN3D_WeightedChebishev_solver.prototxt 2>&1 | tee caffe_model/WeightedChebishev/model_train_${p}_f${fixed_pNorm}_${beta}_${alpha}.log
    done

done

