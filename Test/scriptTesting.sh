#!/bin/bash
# Script para ejecutar el test de SRCNN3D según el artículo

# Creo variable con la ruta hacia las imágenes
Dataset=/home/karlkhader/Documents/deepBrain-master/MPRAGE/TestImages
Model=caffe_model/Iter_2974

# Ejecuto el test
python demo_SRCNN3D.py -t $Dataset/KKI2009-01-MPRAGE_LR.nii -r KKI2009-01-MPRAGE_LR_SRCNN3D.nii -t $Dataset/KKI2009-02-MPRAGE_LR.nii -r KKI2009-02-MPRAGE_LR_SRCNN3D.nii -t $Dataset/KKI2009-03-MPRAGE_LR.nii -r KKI2009-03-MPRAGE_LR_SRCNN3D.nii -t $Dataset/KKI2009-04-MPRAGE_LR.nii -r KKI2009-04-MPRAGE_LR_SRCNN3D.nii -t $Dataset/KKI2009-05-MPRAGE_LR.nii -r KKI2009-05-MPRAGE_LR_SRCNN3D.nii -m $Model/SRCNN3D_iter_2974.caffemodel -n $Model/SRCNN3D_deploy.prototxt -g True -s '2,2,2'
