#!/bin/bash
# Script para ejecutar el test de SRCNN3D según el artículo
# Variante que ejecuta todas las imágenes de un directorio

# Defino los valores de p a ejecutar
#p_values=(0.5 0.75 1 2 3 4 5 6 7 8 9)
fixed_pNorm=2
p_values=(1.9)
# beta y alpha no tienen por qué sumar uno, luego los pongo separados para libre elección
alpha_values=(1 1 1 1 1 1 1)
beta_values=(0.9 1)

# Creo variable con la ruta hacia las imágenes
Dataset=/home/karlkhader/Documents/deepBrain-master/ICAE/ParameterTuning/Zoom2
#Dataset=/home/karlkhader/Documents/deepBrain-master/TestImages-pNorm/Zoom2
#Dataset=/home/karlkhader/Documents/deepBrain-master/TestImages-pNorm/Zoom3

NumStepsModel=50000

for p in ${p_values[@]}; do

    #for beta in ${beta_values[@]}; do
    for i in ${!beta_values[@]}; do

        beta=${beta_values[$i]}
        alpha=${alpha_values[$i]}

        #Model=/home/karlkhader/Documents/deepBrain-master/SRCNN3D-pNorm/Train/caffe_model/p_${p}/beta_${beta}
        Model=../Train/caffe_model/WeightedChebishev/p_${p}_f${fixed_pNorm}/beta_${beta}_alpha_${alpha}

        # Creo directorio donde guardo resultados
        ResultsPath=$Dataset/WeightedChebishev/Results_${NumStepsModel}/p_${p}_f${fixed_pNorm}/beta_${beta}_alpha_${alpha}
        mkdir -p ${ResultsPath}

        # Ejecuto un bucle para hacer los tests
        FICHEROS=`dir $Dataset/*LR.nii`
        ENTRADA=''
        for fich in $FICHEROS; do
            file=${fich##*/}
            base=${file%%.*}
            ENTRADA="${ENTRADA} -t ${fich} -r ${ResultsPath}/${base}_SRCNN3D.nii"
        done

        # Ejecuto el test
        python demo_SRCNN3D.py $ENTRADA -m ${Model}/SRCNN3D_${p}_${beta}_iter_${NumStepsModel}.caffemodel -n ${Model}/../../../../model/SRCNN3D_deploy.prototxt -g True -s '2,2,2'
        #python demo_SRCNN3D.py $ENTRADA -m ${Model}/SRCNN3D_${p}_${beta}_iter_${NumStepsModel}.caffemodel -n ${Model}/../../../../model/SRCNN3D_deploy.prototxt -g True -s '3,3,3'

    done
done