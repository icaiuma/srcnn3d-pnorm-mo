#!/bin/bash
# Script para ejecutar el test de SRCNN3D según el artículo
# Variante que ejecuta todas las imágenes de un directorio

NetType=WeightedChebishev1.9
#NetType=WLC
#NetType=p_1.9
#NetType=p_2

NumStepsModel=50000
zoom_values=(2 3 4)

Model=../Train/caffe_model/OASIS/${NetType}/train1

for zoom in ${zoom_values[@]}; do

    # Creo variable con la ruta hacia las imágenes
    Dataset=/home/karlkhader/Documents/deepBrain-master/ICAE/TestImages/ExtraDataset/Zoom11${zoom}

    # Creo directorio donde guardo resultados
    ResultsPath=$Dataset/${NetType}/Results_${NumStepsModel}
    mkdir -p ${ResultsPath}

    # Ejecuto un bucle para hacer los tests
    FICHEROS=`dir $Dataset/*LR.nii`
    ENTRADA=''
    for fich in $FICHEROS; do
        file=${fich##*/}
        base=${file%%.*}
        ENTRADA="${ENTRADA} -t ${fich} -r ${ResultsPath}/${base}_SRCNN3D.nii"
    done

    # Ejecuto el test
    python demo_SRCNN3D.py $ENTRADA -m ${Model}/SRCNN3D_train1_iter_${NumStepsModel}.caffemodel -n ${Model}/../../../../model/SRCNN3D_deploy.prototxt -g True -s "1,1,${zoom}"

done
