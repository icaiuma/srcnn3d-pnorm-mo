#!/bin/bash
# Script para ejecutar el test de SRCNN3D según el artículo
# Variante que ejecuta todas las imágenes de un directorio

# Defino los valores de p a ejecutar
#p_values=(0.5 0.75 1 2 3 4 5 6 7 8 9)
p_values=(1.6 1.9)

# Creo variable con la ruta hacia las imágenes
Dataset=/home/karlkhader/Documents/deepBrain-master/TestImages-pNorm/Zoom2
#Dataset=/home/karlkhader/Documents/deepBrain-master/TestImages-pNorm/Zoom3

for p in ${p_values[@]}; do

    NumStepsModel=50000
    Model=/home/karlkhader/Documents/deepBrain-master/SRCNN3D/Train/caffe_model/p_${p}

    # Creo directorio donde guardo resultados
    mkdir -p $Dataset/Results_${NumStepsModel}/p_${p}

    # Ejecuto un bucle para hacer los tests
    FICHEROS=`dir $Dataset/*.nii`
    ENTRADA=''
    for fich in $FICHEROS; do
        file=${fich##*/}
        base=${file%%.*}
        ENTRADA="${ENTRADA} -t ${fich} -r ${Dataset}/Results_${NumStepsModel}/p_${p}/${base}_SRCNN3D.nii"
    done

    # Ejecuto el test
    python demo_SRCNN3D.py $ENTRADA -m ${Model}/SRCNN3D_${p}_iter_${NumStepsModel}.caffemodel -n ${Model}/../../model/SRCNN3D_deploy.prototxt -g True -s '2,2,2'
    #python demo_SRCNN3D.py $ENTRADA -m ${Model}/SRCNN3D_${p}_iter_${NumStepsModel}.caffemodel -n ${Model}/../../model/SRCNN3D_deploy.prototxt -g True -s '3,3,3'

done